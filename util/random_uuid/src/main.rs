use structopt::StructOpt;
use uuid::Uuid;

use std::iter;

#[derive(Debug, StructOpt)]
#[structopt(name = "random_uuid", about = "generates random uuids")]
struct Opt {
    /// Number of uuids to generate
    #[structopt(short, long, default_value = "1")]
    count: usize,
    /// Show only the u128 format
    #[structopt(short = "u", long = "u128")]
    decimal: bool,
    /// Show only the hex format
    #[structopt(short = "x", long = "hex")]
    hex: bool,
}

fn main() {
    let opt = Opt::from_args();
    // branch on count, use different formatting for multiple uuids
    if opt.count <= 1 {
        let uuid = Uuid::new_v4();
        match (opt.decimal, opt.hex) {
            (true, true) => println!("Error: can only specify one of -u/--u128 and -x/--hex"),
            (true, false) => println!("{}_u128", uuid.as_u128()),
            (false, true) => println!("{}", uuid.to_hyphenated()),
            (false, false) => {
                println!("{}_u128", uuid.as_u128());
                println!("{}", uuid.to_hyphenated());
            }
        }
    } else {
        let uuids = iter::repeat_with(Uuid::new_v4).take(opt.count).enumerate();
        for (i, uuid) in uuids {
            println!("{: >3}:", i);
            match (opt.decimal, opt.hex) {
                (true, true) => {
                    println!("Error: can only specify one of -u/--u128 and -x/--hex");
                    break;
                }
                (true, false) => println!("  - {}_u128", uuid.as_u128()),
                (false, true) => println!("  - {}", uuid.to_hyphenated()),
                (false, false) => {
                    println!("  - {}_u128", uuid.as_u128());
                    println!("  - {}", uuid.to_hyphenated());
                }
            }
        }
    }
}
