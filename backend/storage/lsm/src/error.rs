//! Error type for the LSM module
use snafu::Snafu;

/// Describes things that can go wrong when operating on an LSM
#[derive(Debug, Snafu)]
#[snafu(visibility = "pub(crate)")]
pub enum Error {
    /// Attempted to parse a segment, but it had mismatched lengths
    SegmentMismatchedLengths,
    /// Value to large to possibly fit into memory
    #[snafu(display("Value to large to fit into memory, size of value: {}", size))]
    ValueLargerThanMemory {
        /// The underlying integer conversion error
        source: std::num::TryFromIntError,
        /// The size of the value trying to be represented
        size: u128,
    },
    /// Failed to allocate a memory map
    MemMapFailed {
        /// The underlying memory map fault
        source: vmap::Error,
    },
    /// File was missing header
    FileMissingHeader,
    /// File had a malformed segment
    MalformedSegment,
    /// Index was inconsistent
    InconsistentIndex,
    /// Failed to flush to disk
    FlushFailed {
        /// The underlying io error
        source: vmap::Error,
    },
    /// Failed to serialize data
    SerFailure {
        /// The underlying error
        source: serde_cbor::Error,
    },
}
