//! A raw, internal view of the database
use serde::de::{Deserialize, DeserializeOwned};
use serde::Serialize;
use serde_cbor::{de::from_slice, ser::to_vec, Deserializer};
use snafu::ResultExt;
use uuid::Uuid;
use vmap::{Map, MapMut};

use std::{
    collections::{BTreeMap, HashMap},
    fs::File,
};

use crate::error::{Error, MemMapFailed, SerFailure};
use crate::traits::HasTable;

pub mod header;
pub mod segment;

use header::Header;

use self::segment::{OwnedSegment, RawSegment};

/// Descriptor and cache for a table
#[derive(Debug)]
struct RawTable {
    /// The key type for this table
    key_type: Uuid,
    /// The value type for this table
    value_type: Uuid,
    /// The index of values to their segment offset
    index: HashMap<Vec<u8>, usize>,
    /// The currently in flight k/v pairs, waiting to be flushed
    cache: BTreeMap<Vec<u8>, Vec<u8>>,
    /// Indicates that this table was newly created
    ///
    /// As the format allows using an empty segment to 'reserve' a table, this flag is used to only
    /// write an empty segment to disk if it was freshly created.
    fresh: bool,
}

/// The underlying, internal database
#[derive(Debug)]
pub struct RawLSM {
    /// Memory mapped handle to the underlying file
    map: Map,
    /// Optional underlying file
    file: Option<File>,
    /// The header data
    header: Header,
    /// The offset of the start of data within the file
    start_of_data: usize,
    /// The offset of the byte immediately following the end of data
    start_of_free: usize,
    /// The map of tables
    tables: HashMap<(Uuid, Uuid), RawTable>,
}

impl RawLSM {
    /// Creates a new, anonymous, memory backed instance
    ///
    /// # Errors
    ///
    /// Will return `Err(Error::MemMapFailed)` if the instance can not be mapped
    pub fn new_transient() -> Result<RawLSM, Error> {
        // First create a header
        let header = Header::current();
        let encoded = to_vec(&header).expect("Failed safe serialize");
        // Create the map
        let mut map_mut = MapMut::with_options()
            .len(encoded.len())
            .alloc()
            .context(MemMapFailed)?;
        // Write the header
        (&mut map_mut[..encoded.len()]).copy_from_slice(&encoded);
        let map = map_mut
            .into_map()
            .expect("Failed to convert map to immutable");
        Self::read_map(map, None)
    }

    /// Internal method for reading an existing map
    fn read_map(map: Map, file: Option<File>) -> Result<RawLSM, Error> {
        let mut deser = Deserializer::from_slice(&map);
        // Deserialize the header
        let header: Header =
            Deserialize::deserialize(&mut deser).map_err(|_| Error::FileMissingHeader)?;
        let start_of_data = deser.byte_offset();
        let mut tables = HashMap::new();
        let mut offset = start_of_data;
        // Parse each segment
        while let Ok(segment) = <RawSegment<'_> as Deserialize>::deserialize(&mut deser) {
            // Get or create the table matching this segment
            let table = tables
                .entry((segment.key_type, segment.value_type))
                .or_insert_with(|| RawTable {
                    key_type: segment.key_type,
                    value_type: segment.value_type,
                    index: HashMap::new(),
                    cache: BTreeMap::new(),
                    fresh: false,
                });
            // Parse the segment
            let borrowed_segment = segment.to_full()?;
            for (key, _value) in borrowed_segment.pairs {
                // Update the index for this key in this table
                table.index.insert(key.to_vec(), offset);
            }
            // Update the offset
            offset = deser.byte_offset();
        }
        Ok(RawLSM {
            map,
            file,
            header,
            start_of_data,
            start_of_free: offset,
            tables,
        })
    }

    /// Returns a reference to the `RawLSM`'s [`Header`] struct
    pub fn header(&self) -> &Header {
        &self.header
    }

    /// Decomposes the `RawLSM` into its component file and memory map
    pub fn into_inner(self) -> (Map, Option<File>) {
        (self.map, self.file)
    }

    /// Internal method for getting or creating a table
    fn get_or_create_table<K: HasTable, V: HasTable>(&mut self) -> &mut RawTable {
        let key_type = K::UUID;
        let value_type = V::UUID;
        self.tables
            .entry((key_type, value_type))
            .or_insert_with(|| RawTable {
                key_type,
                value_type,
                index: HashMap::new(),
                cache: BTreeMap::new(),
                fresh: true,
            })
    }

    /// Ensure that a table exists
    pub fn ensure_table<K: HasTable, V: HasTable>(&mut self) {
        // Delegate to internal method
        self.get_or_create_table::<K, V>();
    }

    /// Insert a key/value pair into the map
    ///
    /// # Errors
    ///
    /// Will return `Err(Error::SerFailure)` if either the user provided key or value fail to serialize
    pub fn insert<K: HasTable + Serialize, V: HasTable + Serialize>(
        &mut self,
        key: &K,
        value: &V,
    ) -> Result<(), Error> {
        let table = self.get_or_create_table::<K, V>();
        let key = to_vec(key).context(SerFailure)?;
        let value = to_vec(value).context(SerFailure)?;
        table.cache.insert(key, value);
        Ok(())
    }

    /// Look up a key/value pair from the map
    ///
    /// # Errors
    ///
    /// Will return `Err(Error::InconsistentIndex)` if the index contains an incorrect reference, or
    /// `Err(Error::SerFailure)` if either the data is corrupt, or the user provided key value fails to
    /// serialize.
    pub fn get<K: HasTable + Serialize, V: HasTable + DeserializeOwned>(
        &self,
        key: &K,
    ) -> Result<Option<V>, Error> {
        // First, check to see if we have the table
        if let Some(table) = self.tables.get(&(K::UUID, V::UUID)) {
            // First, check the cache
            let key_bytes = to_vec(key).context(SerFailure)?;
            if let Some(value) = table.cache.get(&key_bytes) {
                let value = from_slice(value).context(SerFailure)?;
                Ok(Some(value))
            } else {
                // Check the index
                if let Some(offset) = table.index.get(&key_bytes) {
                    let mut deser = Deserializer::from_slice(&self.map[*offset..]);
                    let raw: RawSegment<'_> =
                        <RawSegment<'_> as Deserialize>::deserialize(&mut deser)
                            .context(SerFailure)?;
                    let parsed = raw.to_full()?;
                    if let Ok(index) = parsed
                        .pairs
                        .binary_search_by_key(&&key_bytes[..], |(key, _)| *key)
                    {
                        let (_, value_bytes) = parsed.pairs[index];
                        let value = from_slice(value_bytes).context(SerFailure)?;
                        Ok(Some(value))
                    } else {
                        Err(Error::InconsistentIndex)
                    }
                } else {
                    Ok(None)
                }
            }
        } else {
            Ok(None)
        }
    }

    /// Flush the current data to the backing store
    ///
    /// # Errors
    ///
    /// Will return `Err(Error::FlushFailed)` if an underlying IO error prevents the flush from succeeding.
    #[allow(clippy::missing_panics_doc)] // Contains only a single safe unwrap
    pub fn flush(&mut self) -> Result<(), Error> {
        // First, iterate through the tables and find out which ones have changed state
        let changed_tables = self
            .tables
            .iter_mut()
            .filter(|(_, x)| x.fresh || !x.cache.is_empty());
        let mut segments = Vec::new();
        // Generate an owned segment for each table
        for ((key_type, value_type), table) in changed_tables {
            // Steal its cache
            let mut pairs = BTreeMap::new();
            std::mem::swap(&mut pairs, &mut table.cache);
            // The table is no longer fresh
            table.fresh = false;
            segments.push(OwnedSegment {
                key_type: *key_type,
                value_type: *value_type,
                pairs,
            });
        }
        // Create a place to put all our bytes
        let mut bytes = Vec::new();
        // Serialize all the segments
        for segment in segments {
            let local_offset = bytes.len() + self.start_of_free;
            // Write the bytes
            let raw = segment.to_raw();
            let borrowed = raw.to_borrowed();
            let tmp = to_vec(&borrowed).expect("Infallible serialization failed");
            bytes.extend_from_slice(&tmp);
            // Update the table index
            let table = self
                .tables
                .get_mut(&(segment.key_type, segment.value_type))
                .unwrap(); // This is safe, since the segment comes from a table
            for key in segment.pairs.keys() {
                table.index.insert(key.clone(), local_offset);
            }
        }
        // Now we need to branch on if we have a file
        if let Some(_file) = self.file.as_ref() {
            todo!()
        } else {
            // Make a new map, and copy the contents
            let new_start_of_free = self.start_of_free + bytes.len();
            let mut map_mut = MapMut::with_options()
                .len(new_start_of_free)
                .alloc()
                .context(MemMapFailed)?;
            // Copy the existing map into it
            (&mut map_mut[..self.start_of_free]).copy_from_slice(&self.map[..self.start_of_free]);
            // Copy in the new data
            (&mut map_mut[self.start_of_free..new_start_of_free]).copy_from_slice(&bytes);
            // Blit it over to a read-only map and replace
            let map: Map = map_mut
                .into_map()
                .expect("Failed to convert map to immutable");
            self.map = map;
            self.start_of_free = new_start_of_free;
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde::{Deserialize, Serialize};

    #[derive(Serialize, Deserialize, Debug)]
    struct TestKey(u64);

    impl HasTable for TestKey {
        const UUID: Uuid = Uuid::from_u128(40646636939501403018275778015937099879_u128);
    }

    #[derive(Serialize, Deserialize, Debug)]
    struct TestValue(u64);

    impl HasTable for TestValue {
        const UUID: Uuid = Uuid::from_u128(83163149886979913607733546850976996842_u128);
    }

    #[test]
    fn anon_doenst_crash() {
        let item = RawLSM::new_transient().unwrap();
        let (map, _) = item.into_inner();
        println!("{:02x?}", &map[..]);
    }

    #[test]
    fn anon_create_table() {
        let mut item = RawLSM::new_transient().unwrap();
        item.ensure_table::<TestKey, TestValue>();
        println!("{:?}", item);
        item.flush().unwrap();
        println!("{:?}", item);
        let (map, _) = item.into_inner();
        println!("{:02x?}", &map[..]);
    }

    #[test]
    fn anon_insert_retrieve() {
        let mut item = RawLSM::new_transient().unwrap();
        // Insert a couple of values
        item.insert(&TestKey(1), &TestValue(42)).unwrap();
        item.insert(&TestKey(2), &TestValue(1024)).unwrap();
        println!("{:?}", item);
        // check the values from cache
        assert!(matches!(item.get(&TestKey(1)), Ok(Some(TestValue(42)))));
        assert!(matches!(item.get(&TestKey(2)), Ok(Some(TestValue(1024)))));
        // Nonsense value should get nothing back
        assert!(matches!(
            item.get::<TestKey, TestValue>(&TestKey(3)),
            Ok(None)
        ));
        // Flush the map
        item.flush().unwrap();
        println!("{:?}", item);
        println!("{:02x?}", &item.map[..]);
        // Check the values from the backing store
        let test = item.get::<TestKey, TestValue>(&TestKey(1));
        println!("{:?}", test);
        assert!(matches!(item.get(&TestKey(1)), Ok(Some(TestValue(42)))));
        assert!(matches!(item.get(&TestKey(2)), Ok(Some(TestValue(1024)))));
        // Nonsense value should still get nothing back
        assert!(matches!(
            item.get::<TestKey, TestValue>(&TestKey(3)),
            Ok(None)
        ));
        // Update a value
        item.insert(&TestKey(1), &TestValue(43)).unwrap();
        println!("{:?}", item);
        // Check from cache
        assert!(matches!(item.get(&TestKey(1)), Ok(Some(TestValue(43)))));
        // Flush and check again
        item.flush().unwrap();
        assert!(matches!(item.get(&TestKey(1)), Ok(Some(TestValue(43)))));
        // Unchanged value should still be the same
        assert!(matches!(item.get(&TestKey(2)), Ok(Some(TestValue(1024)))));
        println!("{:?}", item);
        println!("{:02x?}", &item.map[..]);
        // We should be able to deconstruct the lsm and use the map to rebuild it
        let (map, _) = item.into_inner();
        let item = RawLSM::read_map(map, None).unwrap();
        // Everything should still check out
        assert!(matches!(item.get(&TestKey(1)), Ok(Some(TestValue(43)))));
        assert!(matches!(item.get(&TestKey(2)), Ok(Some(TestValue(1024)))));
        // Nonsense value should still get nothing back
        assert!(matches!(
            item.get::<TestKey, TestValue>(&TestKey(3)),
            Ok(None)
        ));
        println!("{:?}", item);
        println!("{:02x?}", &item.map[..]);
    }
}
