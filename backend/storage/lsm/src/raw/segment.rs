//! Types used for mapping the on-disk segments
use std::{collections::BTreeMap, convert::TryInto};

use serde::{Deserialize, Serialize};
use snafu::{ensure, ResultExt};
use uuid::Uuid;

use crate::error::{Error, SegmentMismatchedLengths, ValueLargerThanMemory};

/// Raw Encoding of a segment in the map
#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct RawSegment<'a> {
    /// The [Uuid] of the key's type, as defined by [HasTable](crate::traits::HasTable)
    pub key_type: Uuid,
    /// The [Uuid] of the value's type, as defined by [HasTable](crate::traits::HasTable)
    pub value_type: Uuid,
    /// The raw storage for the keys
    #[serde(with = "serde_bytes")]
    pub keys: &'a [u8],
    /// The lengths of each key
    pub key_lengths: Vec<u32>,
    /// The raw storage for the values
    #[serde(with = "serde_bytes")]
    pub values: &'a [u8],
    /// lengths of each value
    pub value_lengths: Vec<u32>,
}

impl<'a> RawSegment<'a> {
    /// Converts this segment to the full borrowed format
    ///
    /// # Errors
    ///
    /// Will return `Err(Error::SegmentMismatchedLengths)` if the input is malformed
    pub fn to_full(&self) -> Result<BorrowedSegment<'_>, Error> {
        let key_type = self.key_type;
        let value_type = self.value_type;
        // Make sure our key and value maps are the same length
        ensure!(
            self.key_lengths.len() == self.value_lengths.len(),
            SegmentMismatchedLengths
        );

        // Make sure the lengths add up
        let test_key_lengths = self.key_lengths.iter().copied().sum::<u32>();
        ensure!(
            TryInto::<usize>::try_into(test_key_lengths).context(ValueLargerThanMemory {
                size: test_key_lengths
            })? == self.keys.len(),
            SegmentMismatchedLengths
        );
        let test_value_lengths = self.value_lengths.iter().copied().sum::<u32>();
        ensure!(
            TryInto::<usize>::try_into(test_value_lengths).context(ValueLargerThanMemory {
                size: test_value_lengths
            })? == self.values.len(),
            SegmentMismatchedLengths
        );

        let mut pairs = Vec::new();
        let mut key_slice: &[u8] = self.keys;
        let mut value_slice: &[u8] = self.values;
        for (key_len, value_len) in self
            .key_lengths
            .iter()
            .copied()
            .zip(self.value_lengths.iter().copied())
        {
            let (key, key_rest) = key_slice.split_at(
                key_len
                    .try_into()
                    .context(ValueLargerThanMemory { size: key_len })?,
            );
            key_slice = key_rest;
            let (value, value_rest) = value_slice.split_at(
                value_len
                    .try_into()
                    .context(ValueLargerThanMemory { size: value_len })?,
            );
            value_slice = value_rest;
            pairs.push((key, value));
        }

        Ok(BorrowedSegment {
            key_type,
            value_type,
            pairs,
        })
    }
    /// Converts this segment to the owned raw format
    pub fn to_owned(&self) -> OwnedRawSegment {
        OwnedRawSegment {
            key_type: self.key_type,
            value_type: self.value_type,
            keys: self.keys.to_vec(),
            key_lengths: self.key_lengths.clone(),
            values: self.values.to_vec(),
            value_lengths: self.value_lengths.clone(),
        }
    }
}

/// Decoded segment from the map, with borrowed data
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct BorrowedSegment<'a> {
    /// The [Uuid] of the key's type, as defined by [HasTable](crate::traits::HasTable)
    pub key_type: Uuid,
    /// The [Uuid] of the value's type, as defined by [HasTable](crate::traits::HasTable)
    pub value_type: Uuid,
    /// The (key, value) pairs in this segment
    pub pairs: Vec<(&'a [u8], &'a [u8])>,
}

impl<'a> BorrowedSegment<'a> {
    /// Converts this [`BorrowedSegment`] to the full, owned format
    pub fn to_owned(&self) -> OwnedSegment {
        let key_type = self.key_type;
        let value_type = self.value_type;
        let mut pairs: BTreeMap<Vec<u8>, Vec<u8>> = BTreeMap::new();
        for (k, v) in &self.pairs {
            pairs.insert(k.to_vec(), v.to_vec());
        }

        OwnedSegment {
            key_type,
            value_type,
            pairs,
        }
    }
}

/// Raw encoding of a map segment, with owned data
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct OwnedRawSegment {
    /// The [Uuid] of the key's type, as defined by [HasTable](crate::traits::HasTable)
    pub key_type: Uuid,
    /// The [Uuid] of the value's type, as defined by [HasTable](crate::traits::HasTable)
    pub value_type: Uuid,
    /// The raw storage for the keys
    pub keys: Vec<u8>,
    /// The lengths of each key
    pub key_lengths: Vec<u32>,
    /// The raw storage for the values
    pub values: Vec<u8>,
    /// lengths of each value
    pub value_lengths: Vec<u32>,
}

impl OwnedRawSegment {
    /// Converts this segment to full format
    ///
    /// # Errors
    ///
    /// Will return an `Err(Error::SegmentMismatchedLengths)` if the input is malformed
    pub fn to_full(&self) -> Result<OwnedSegment, Error> {
        let key_type = self.key_type;
        let value_type = self.value_type;
        // Make sure our key and value maps are the same length
        ensure!(
            self.key_lengths.len() == self.value_lengths.len(),
            SegmentMismatchedLengths
        );
        // Make sure the lengths add up
        let test_key_lengths = self.key_lengths.iter().copied().sum::<u32>();
        ensure!(
            TryInto::<usize>::try_into(test_key_lengths).context(ValueLargerThanMemory {
                size: test_key_lengths
            })? == self.keys.len(),
            SegmentMismatchedLengths
        );
        let test_value_lengths = self.value_lengths.iter().copied().sum::<u32>();
        ensure!(
            TryInto::<usize>::try_into(test_value_lengths).context(ValueLargerThanMemory {
                size: test_value_lengths
            })? == self.values.len(),
            SegmentMismatchedLengths
        );
        let mut pairs = BTreeMap::<Vec<u8>, Vec<u8>>::new();
        let mut key_slice: &[u8] = &self.keys;
        let mut value_slice: &[u8] = &self.values;
        for (key_len, value_len) in self
            .key_lengths
            .iter()
            .copied()
            .zip(self.value_lengths.iter().copied())
        {
            let (key, key_rest) = key_slice.split_at(
                key_len
                    .try_into()
                    .context(ValueLargerThanMemory { size: key_len })?,
            );
            key_slice = key_rest;
            let (value, value_rest) = value_slice.split_at(
                value_len
                    .try_into()
                    .context(ValueLargerThanMemory { size: value_len })?,
            );
            value_slice = value_rest;
            pairs.insert(key.to_vec(), value.to_vec());
        }
        Ok(OwnedSegment {
            key_type,
            value_type,
            pairs,
        })
    }
    /// Converts this segment to borrowed raw format
    pub fn to_borrowed(&self) -> RawSegment<'_> {
        RawSegment {
            key_type: self.key_type,
            value_type: self.value_type,
            keys: &self.keys,
            key_lengths: self.key_lengths.clone(),
            values: &self.values,
            value_lengths: self.value_lengths.clone(),
        }
    }
}

/// Map segment with owned data
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct OwnedSegment {
    /// The [Uuid] of the key's type, as defined by [HasTable](crate::traits::HasTable)
    pub key_type: Uuid,
    /// The [Uuid] of the value's type, as defined by [HasTable](crate::traits::HasTable)
    pub value_type: Uuid,
    /// The (key, value) pairs in this segment
    pub pairs: BTreeMap<Vec<u8>, Vec<u8>>,
}

impl OwnedSegment {
    /// Converts this [`OwnedSegment`] to raw format
    #[allow(clippy::missing_panics_doc)] // This can only panic if usize is smaller than a u32,
                                         // which we don't support
    pub fn to_raw(&self) -> OwnedRawSegment {
        let key_type = self.key_type;
        let value_type = self.value_type;
        // Create store locations
        let mut keys = Vec::<u8>::new();
        let mut key_lengths = Vec::<u32>::new();
        let mut values = Vec::<u8>::new();
        let mut value_lengths = Vec::<u32>::new();
        for (k, v) in &self.pairs {
            // These unwraps are safe because we don't support anything below 32bit
            key_lengths.push(k.len().try_into().unwrap());
            value_lengths.push(v.len().try_into().unwrap());
            keys.extend_from_slice(k);
            values.extend_from_slice(v);
        }

        OwnedRawSegment {
            key_type,
            value_type,
            keys,
            key_lengths,
            values,
            value_lengths,
        }
    }

    /// Converts this [`OwnedSegment`] to the full borrowed format
    pub fn to_borrowed(&self) -> BorrowedSegment<'_> {
        let key_type = self.key_type;
        let value_type = self.value_type;
        // Create store location
        let mut pairs: Vec<(&[u8], &[u8])> = Vec::new();
        for (k, v) in &self.pairs {
            pairs.push((k, v));
        }

        BorrowedSegment {
            key_type,
            value_type,
            pairs,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    prop_compose! {
        fn owned_segment()
            (key_type in any::<[u8;16]>(),
             value_type in any::<[u8;16]>(),
             pairs in any::<BTreeMap<Vec<u8>, Vec<u8>>>())
             -> OwnedSegment {
                OwnedSegment {
                    key_type: uuid::Builder::from_bytes(key_type)
                        .set_variant(uuid::Variant::RFC4122)
                        .set_version(uuid::Version::Random)
                        .build(),
                    value_type: uuid::Builder::from_bytes(value_type)
                        .set_variant(uuid::Variant::RFC4122)
                        .set_version(uuid::Version::Random)
                        .build(),
                    pairs,
                }
        }
    }

    proptest! {
        #[test]
        fn owned_to_raw_and_back(x in owned_segment()) {
            let raw = x.to_raw();
            let output = raw.to_full();
            prop_assert!(output.is_ok());
            prop_assert_eq!(output.unwrap(), x);
        }

        #[test]
        fn owned_to_borrowed_and_back(x in owned_segment()) {
            let borrowed = x.to_borrowed();
            let output = borrowed.to_owned();
            prop_assert_eq!(output, x);
        }

        #[test]
        fn owned_raw_to_raw_and_back(x in owned_segment()) {
            let owned_raw = x.to_raw();
            let borrowed_raw = owned_raw.to_borrowed();
            let res = borrowed_raw.to_owned();
            prop_assert_eq!(res, owned_raw);
        }

        #[test]
        fn full_round_trip(x in owned_segment()) {
            let owned_raw = x.to_raw();
            let borrowed_raw = owned_raw.to_borrowed();
            let borrowed = borrowed_raw.to_full();
            prop_assert!(borrowed.is_ok());
            prop_assert_eq!(borrowed.unwrap().to_owned(), x);
        }
    }
}
