//! Contains the description of the binary header for a [`RawLSM`](super::RawLsm)
use chrono::{offset::Utc, DateTime};
use semver::Version;
use serde::{Deserialize, Serialize};

/// The binary header for a [`RawLSM`](super::RawLsm)
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Header {
    /// The version of the library this file was initially written with
    version: Version,
    /// The date/time this world file was created at
    creation: DateTime<Utc>,
}

impl Header {
    /// Creates a new header with the current version and date/time
    pub fn current() -> Self {
        Header {
            version: crate::VERSION.clone(),
            creation: Utc::now(),
        }
    }
}
