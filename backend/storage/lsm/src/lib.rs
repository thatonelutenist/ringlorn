#![warn(
    clippy::all,
    clippy::pedantic,
    rust_2018_idioms,
    missing_docs,
    clippy::missing_docs_in_private_items
)]
#![allow(clippy::must_use_candidate, clippy::module_name_repetitions)]

//! # Type Based Log Structured Merge Trees Database
//!
//! This module provides a single file, log structured merge tree based data store supporting multiple
//! tables, where the type of a value is used to determine which table is being accessed.

use lazy_static::lazy_static;

pub mod error;
pub mod raw;
pub mod traits;

// Re-export types that appear in the public api
pub use uuid;

/// The string form of the version of this library
///
/// Used for generating headers
const VERSION_STRING: &str = env!("CARGO_PKG_VERSION");

lazy_static! {
    /// The semver version of this library
    pub(crate) static ref VERSION: semver::Version = {
        semver::Version::parse(VERSION_STRING).expect("Unable to parse version from cargo")
    };
}
