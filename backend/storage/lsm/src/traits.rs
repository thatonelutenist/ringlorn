//! # Database traits
//!
//! This module defines various traits that types must implement to be stored in the database
use uuid::Uuid;

/// Maps a type to a universally unique identifier
pub trait HasTable {
    /// The universally unique identifier for this type
    const UUID: Uuid;
}
