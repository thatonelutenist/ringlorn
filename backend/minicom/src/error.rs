use snafu::Snafu;

use crate::types::Kind;

/// Errors that can occur when interacting with minicom
#[derive(Debug, Snafu, PartialEq, Eq)]
pub enum Error {
    /// A general type mismatch
    #[snafu(display("Type mismatch: Expected - {:?}, Actual - {:?}", expected, actual))]
    TypeMismatch {
        /// The expected type
        expected: Kind,
        /// The found type
        actual: Kind,
    },
    /// An integer overflow occurred while attempting to convert from [`crate::types::Value`] to a concrete
    /// integer type
    IntegerOverflow,
    /// A floating point overflow occurred while attempting to convert from [`crate::types::Value`] to a
    /// concrete floating point type
    FloatingPointOverflow,
    /// A vector contained a value of the wrong type
    #[snafu(display(
        "Mismatch in types in Vector: Expected - {:?} Found - {:?} Index - {} Value",
        expected,
        actual,
        index
    ))]
    HeterogenousVector {
        /// The expected type
        expected: Kind,
        /// The actual type
        actual: Kind,
        /// The index of the mismatch
        index: usize,
    },
    /// A map contained a pair of the wrong type
    HeterogenousMap {
        /// The expected type of the key
        expected_key: Kind,
        /// The expected type of the value
        expected_value: Kind,
        /// The actual type of the key
        actual_key: Kind,
        /// The actual type of the value
        acutal_value: Kind,
    },
}
