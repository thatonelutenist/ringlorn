use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};
use uuid::Uuid;

/// Conversion traits for types that can be made into a [`Value`]
pub mod conversion;
/// Float wrapper type that implements `Eq` + `Ord` + `Hash`
pub mod float_wrapper;

use float_wrapper::WrappedFloat;

/// Types a value can have
#[derive(Debug, PartialEq, Eq, Serialize, Deserialize, Hash, PartialOrd, Ord, Clone)]
pub enum Kind {
    /// A signed integer value
    Int,
    /// An unsigned integer value
    UInt,
    /// A boolean value
    Bool,
    /// A single character
    Char,
    /// A floating point number
    Float,
    /// The single valued type
    Void,
    /// A string of text
    String,
    /// A vector of other values
    Vec(Box<Kind>),
    /// A map
    Map {
        /// The type of the keys of the map
        key: Box<Kind>,
        /// The type of the values of the map
        value: Box<Kind>,
    },
    /// A universally unique identifier
    Uuid,
}

/// An actual value itself
#[derive(Debug, PartialEq, Eq, Serialize, Deserialize, Hash, PartialOrd, Ord, Clone)]
pub enum Value {
    /// A signed integer value
    Int(i64),
    /// An unsigned integer value
    UInt(u64),
    /// A boolean value
    Bool(bool),
    /// A char
    Char(char),
    /// A floating point number
    Float(WrappedFloat),
    /// The single valued type
    Void,
    /// A string of text
    String(String),
    /// A vector of other values
    Vec {
        /// The type of the values in this vector
        kind: Kind,
        /// The actual values in this vector.
        ///
        /// These values _must_ all be of the same type.
        values: Vec<Value>,
    },
    /// A map
    Map {
        /// The type of the keys of the map
        key: Kind,
        /// The type of the values of the map
        value: Kind,
        /// The actual map itself
        map: BTreeMap<Value, Value>,
    },
    /// A universally unique identifier
    Uuid(Uuid),
}

impl Value {
    /// Returns the [`Kind`] of this value
    pub fn kind(&self) -> Kind {
        match self {
            Value::Int(_) => Kind::Int,
            Value::UInt(_) => Kind::UInt,
            Value::String(_) => Kind::String,
            Value::Vec { kind, .. } => Kind::Vec(Box::new(kind.clone())),
            Value::Map { key, value, .. } => Kind::Map {
                key: Box::new(key.clone()),
                value: Box::new(value.clone()),
            },
            Value::Uuid(_) => Kind::Uuid,
            Value::Bool(_) => Kind::Bool,
            Value::Char(_) => Kind::Char,
            Value::Float(_) => Kind::Float,
            Value::Void => Kind::Void,
        }
    }
}

/// Specification of an argument
#[derive(Debug, PartialEq, Eq, Serialize, Deserialize, Hash, PartialOrd, Ord, Clone)]
pub struct ArgumentSpec {
    /// The name of this argument
    name: String,
    /// The kind of this argument
    kind: Kind,
}

/// An argument with a value associated with it
#[derive(Debug, PartialEq, Eq, Serialize, Deserialize, Hash, PartialOrd, Ord, Clone)]
pub struct Argument {
    /// The specification for this argument
    spec: ArgumentSpec,
    /// The value this argument has taken
    value: Value,
}
