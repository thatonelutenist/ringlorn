use std::{
    collections::{BTreeMap, HashMap},
    hash::{BuildHasher, Hash},
};

use crate::{
    error::Error,
    types::{
        conversion::{FromValue, ToValue},
        Kind, Value,
    },
};

impl<V: ToValue> ToValue for Vec<V> {
    fn kind() -> Kind {
        Kind::Vec(Box::new(V::kind()))
    }

    fn to_value(self) -> Value {
        Value::Vec {
            kind: V::kind(),
            values: self.into_iter().map(ToValue::to_value).collect(),
        }
    }
}

impl<V: FromValue> FromValue for Vec<V> {
    fn from_value(input: Value) -> Result<Self, Error>
    where
        Self: Sized,
    {
        if let Value::Vec { kind, values } = input {
            if kind == V::kind() {
                values.into_iter().map(V::from_value).collect()
            } else {
                Err(Error::TypeMismatch {
                    expected: Kind::Vec(Box::new(V::kind())),
                    actual: Kind::Vec(Box::new(kind)),
                })
            }
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Vec(Box::new(V::kind())),
                actual: input.kind(),
            })
        }
    }
}

impl<K: ToValue, V: ToValue> ToValue for BTreeMap<K, V> {
    fn kind() -> Kind {
        Kind::Map {
            key: Box::new(K::kind()),
            value: Box::new(V::kind()),
        }
    }

    fn to_value(self) -> Value {
        Value::Map {
            key: K::kind(),
            value: V::kind(),
            map: self
                .into_iter()
                .map(|(k, v)| (k.to_value(), v.to_value()))
                .collect(),
        }
    }
}

impl<K: FromValue + Ord, V: FromValue> FromValue for BTreeMap<K, V> {
    fn from_value(input: Value) -> Result<Self, Error>
    where
        Self: Sized,
    {
        if let Value::Map { key, value, map } = input {
            if key == K::kind() && value == K::kind() {
                map.into_iter()
                    .map(|(k, v)| match K::from_value(k) {
                        Ok(k) => V::from_value(v).map(|v| (k, v)),
                        Err(e) => Err(e),
                    })
                    .collect()
            } else {
                Err(Error::TypeMismatch {
                    expected: Kind::Map {
                        key: Box::new(K::kind()),
                        value: Box::new(V::kind()),
                    },
                    actual: Kind::Map {
                        key: Box::new(key),
                        value: Box::new(value),
                    },
                })
            }
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Map {
                    key: Box::new(K::kind()),
                    value: Box::new(V::kind()),
                },
                actual: input.kind(),
            })
        }
    }
}

impl<K: ToValue, V: ToValue, S: BuildHasher> ToValue for HashMap<K, V, S> {
    fn kind() -> Kind {
        Kind::Map {
            key: Box::new(K::kind()),
            value: Box::new(V::kind()),
        }
    }

    fn to_value(self) -> Value {
        Value::Map {
            key: K::kind(),
            value: V::kind(),
            map: self
                .into_iter()
                .map(|(k, v)| (k.to_value(), v.to_value()))
                .collect(),
        }
    }
}

impl<K: FromValue + Hash + Eq, V: FromValue, S: BuildHasher + Default> FromValue
    for HashMap<K, V, S>
{
    fn from_value(input: Value) -> Result<Self, Error>
    where
        Self: Sized,
    {
        if let Value::Map { key, value, map } = input {
            if key == K::kind() && value == V::kind() {
                map.into_iter()
                    .map(|(k, v)| match K::from_value(k) {
                        Ok(k) => V::from_value(v).map(|v| (k, v)),
                        Err(e) => Err(e),
                    })
                    .collect()
            } else {
                Err(Error::TypeMismatch {
                    expected: Kind::Map {
                        key: Box::new(K::kind()),
                        value: Box::new(V::kind()),
                    },
                    actual: Kind::Map {
                        key: Box::new(key),
                        value: Box::new(value),
                    },
                })
            }
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Map {
                    key: Box::new(K::kind()),
                    value: Box::new(V::kind()),
                },
                actual: input.kind(),
            })
        }
    }
}
