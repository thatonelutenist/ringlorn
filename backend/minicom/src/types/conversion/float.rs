use crate::{
    error::Error,
    types::{
        conversion::{FromValue, ToValue},
        float_wrapper::WrappedFloat,
        Kind, Value,
    },
};

impl ToValue for f32 {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Float
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::Float(WrappedFloat(f64::from(self)))
    }
}

impl ToValue for f64 {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Float
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::Float(WrappedFloat(self))
    }
}

impl FromValue for f32 {
    #[inline(always)]
    #[allow(clippy::cast_possible_truncation)]
    fn from_value(input: Value) -> Result<Self, Error>
    where
        Self: Sized,
    {
        if let Value::Float(WrappedFloat(x)) = input {
            Ok(x as f32)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Float,
                actual: input.kind(),
            })
        }
    }
}

impl FromValue for f64 {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, Error>
    where
        Self: Sized,
    {
        if let Value::Float(WrappedFloat(x)) = input {
            Ok(x)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Float,
                actual: input.kind(),
            })
        }
    }
}
