use crate::{
    error::Error,
    types::conversion::{FromValue, ToValue},
};
use proptest::{
    collection::{hash_map, vec},
    prelude::*,
};

use std::{collections::HashMap, convert::TryFrom};

proptest! {
    #[test]
    fn u8_round_trip_happy(a in any::<u8>()) {
        let value = a.to_value();
        let int = u8::from_value(value).unwrap();
        prop_assert_eq!(int, a);
    }
    #[test]
    fn u16_round_trip_happy(a in any::<u16>()) {
        let value = a.to_value();
        let int = u16::from_value(value).unwrap();
        prop_assert_eq!(int, a);
    }
    #[test]
    fn u32_round_trip_happy(a in any::<u32>()) {
        let value = a.to_value();
        let int = u32::from_value(value).unwrap();
        prop_assert_eq!(int, a);
    }
    #[test]
    fn u64_round_trip_happy(a in any::<u64>()) {
        let value = a.to_value();
        let int = u64::from_value(value).unwrap();
        prop_assert_eq!(int, a);
    }

    #[test]
    fn i8_round_trip_happy(a in any::<i8>()) {
        let value = a.to_value();
        let int = i8::from_value(value).unwrap();
        prop_assert_eq!(int, a);
    }
    #[test]
    fn i16_round_trip_happy(a in any::<i16>()) {
        let value = a.to_value();
        let int = i16::from_value(value).unwrap();
        prop_assert_eq!(int, a);
    }
    #[test]
    fn i32_round_trip_happy(a in any::<i32>()) {
        let value = a.to_value();
        let int = i32::from_value(value).unwrap();
        prop_assert_eq!(int, a);
    }
    #[test]
    fn i64_round_trip_happy(a in any::<i64>()) {
        let value = a.to_value();
        let int = i64::from_value(value).unwrap();
        prop_assert_eq!(int, a);
    }

    #[test]
    fn u8_round_trip_sad(a in any::<u64>()) {
        let value = a.to_value();
        let int = u8::from_value(value);
        prop_assert_eq!(int, u8::try_from(a).map_err(|_| Error::IntegerOverflow));
    }
    #[test]
    fn u16_round_trip_sad(a in any::<u64>()) {
        let value = a.to_value();
        let int = u16::from_value(value);
        prop_assert_eq!(int, u16::try_from(a).map_err(|_| Error::IntegerOverflow));
    }
    #[test]
    fn u32_round_trip_sad(a in any::<u64>()) {
        let value = a.to_value();
        let int = u32::from_value(value);
        prop_assert_eq!(int, u32::try_from(a).map_err(|_| Error::IntegerOverflow));
    }
    #[test]
    fn i8_round_trip_sad(a in any::<i64>()) {
        let value = a.to_value();
        let int = i8::from_value(value);
        prop_assert_eq!(int, i8::try_from(a).map_err(|_| Error::IntegerOverflow));
    }
    #[test]
    fn i16_round_trip_sad(a in any::<i64>()) {
        let value = a.to_value();
        let int = i16::from_value(value);
        prop_assert_eq!(int, i16::try_from(a).map_err(|_| Error::IntegerOverflow));
    }
    #[test]
    fn i32_round_trip_sad(a in any::<i64>()) {
        let value = a.to_value();
        let int = i32::from_value(value);
        prop_assert_eq!(int, i32::try_from(a).map_err(|_| Error::IntegerOverflow));
    }

    #[test]
    fn f64_round_trip(a in any::<f64>()) {
        let value = a.to_value();
        let float = f64::from_value(value).unwrap();
        prop_assert_eq!(float.to_bits(), a.to_bits());
    }

    #[test]
    fn vec_round_trip(a in vec(any::<u64>(), 0..100)) {
        let value = a.clone().to_value();
        let vec = Vec::<u64>::from_value(value).unwrap();
        prop_assert_eq!(vec, a);
    }

    #[test]
    fn hashmap_round_trip(a in hash_map(any::<u64>(),vec(any::<u64>(), 0..100),0..100)) {
        let value = a.clone().to_value();
        let hash_map = HashMap::<u64, Vec<u64>>::from_value(value).unwrap();
        prop_assert_eq!(hash_map, a);
    }
}
