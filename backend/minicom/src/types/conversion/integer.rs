use std::convert::TryInto;

use crate::{
    error::Error,
    types::{
        conversion::{FromValue, ToValue},
        Kind, Value,
    },
};

impl ToValue for i8 {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Int
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::Int(i64::from(self))
    }
}

impl ToValue for i16 {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Int
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::Int(i64::from(self))
    }
}

impl ToValue for i32 {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Int
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::Int(i64::from(self))
    }
}

impl ToValue for i64 {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Int
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::Int(self)
    }
}

impl FromValue for i8 {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, crate::error::Error>
    where
        Self: Sized,
    {
        if let Value::Int(x) = input {
            x.try_into().map_err(|_| Error::IntegerOverflow)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Int,
                actual: input.kind(),
            })
        }
    }
}

impl FromValue for i16 {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, crate::error::Error>
    where
        Self: Sized,
    {
        if let Value::Int(x) = input {
            x.try_into().map_err(|_| Error::IntegerOverflow)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Int,
                actual: input.kind(),
            })
        }
    }
}

impl FromValue for i32 {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, crate::error::Error>
    where
        Self: Sized,
    {
        if let Value::Int(x) = input {
            x.try_into().map_err(|_| Error::IntegerOverflow)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Int,
                actual: input.kind(),
            })
        }
    }
}

impl FromValue for i64 {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, crate::error::Error>
    where
        Self: Sized,
    {
        if let Value::Int(x) = input {
            Ok(x)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Int,
                actual: input.kind(),
            })
        }
    }
}

impl ToValue for u8 {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Int
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::UInt(u64::from(self))
    }
}

impl ToValue for u16 {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Int
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::UInt(u64::from(self))
    }
}

impl ToValue for u32 {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Int
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::UInt(u64::from(self))
    }
}

impl ToValue for u64 {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Int
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::UInt(self)
    }
}

impl FromValue for u8 {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, crate::error::Error>
    where
        Self: Sized,
    {
        if let Value::UInt(x) = input {
            x.try_into().map_err(|_| Error::IntegerOverflow)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::UInt,
                actual: input.kind(),
            })
        }
    }
}

impl FromValue for u16 {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, crate::error::Error>
    where
        Self: Sized,
    {
        if let Value::UInt(x) = input {
            x.try_into().map_err(|_| Error::IntegerOverflow)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::UInt,
                actual: input.kind(),
            })
        }
    }
}

impl FromValue for u32 {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, crate::error::Error>
    where
        Self: Sized,
    {
        if let Value::UInt(x) = input {
            x.try_into().map_err(|_| Error::IntegerOverflow)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::UInt,
                actual: input.kind(),
            })
        }
    }
}

impl FromValue for u64 {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, crate::error::Error>
    where
        Self: Sized,
    {
        if let Value::UInt(x) = input {
            Ok(x)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::UInt,
                actual: input.kind(),
            })
        }
    }
}
