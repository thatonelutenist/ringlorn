use serde::{Deserialize, Serialize};

/// Wrapper around a float that implements more traits for it
///
/// Note: these wrappers aren't entirely sane
#[repr(transparent)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct WrappedFloat(pub(crate) f64);

impl PartialEq for WrappedFloat {
    fn eq(&self, other: &Self) -> bool {
        self.0.to_bits().eq(&other.0.to_bits())
    }
}

impl Eq for WrappedFloat {}

impl PartialOrd for WrappedFloat {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0.to_bits().partial_cmp(&other.0.to_bits())
    }
}

impl Ord for WrappedFloat {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0.to_bits().cmp(&other.0.to_bits())
    }
}

impl std::hash::Hash for WrappedFloat {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.to_bits().hash(state);
    }
}
