#![allow(clippy::inline_always)]
use uuid::Uuid;

use std::marker::Sized;

/// Trait implementations for collections
pub mod collections;
/// Trait implementations for floating point primitives
pub mod float;
/// Trait implementations for integer primitives
pub mod integer;
/// Unit tests
#[cfg(test)]
mod tests;

use crate::{
    error::Error,
    types::{Kind, Value},
};

/// Trait for converting types to a [`Value`]
pub trait ToValue {
    /// The [`Kind`] that this value takes when converted to a [`Value`]
    fn kind() -> Kind;
    /// Converts this value to a [`Value`]
    fn to_value(self) -> Value;
}

/// Trait for converting from a [`Value`] back to its normal type
pub trait FromValue: ToValue {
    /// Attempts to convert from a [`Value`] to an instance of this type
    ///
    /// # Errors
    ///
    /// Returns an `Err(Error::TypeMismatch)` if the [`Value`] is not of the correct type
    fn from_value(input: Value) -> Result<Self, Error>
    where
        Self: Sized;
}

impl ToValue for bool {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Bool
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::Bool(self)
    }
}

impl ToValue for char {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Char
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::Char(self)
    }
}

impl ToValue for () {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Void
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::Void
    }
}

impl ToValue for Uuid {
    #[inline(always)]
    fn kind() -> Kind {
        Kind::Uuid
    }

    #[inline(always)]
    fn to_value(self) -> Value {
        Value::Uuid(self)
    }
}

impl FromValue for bool {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, Error>
    where
        Self: Sized,
    {
        if let Value::Bool(x) = input {
            Ok(x)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Bool,
                actual: input.kind(),
            })
        }
    }
}

impl FromValue for char {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, Error>
    where
        Self: Sized,
    {
        if let Value::Char(x) = input {
            Ok(x)
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Char,
                actual: input.kind(),
            })
        }
    }
}

impl FromValue for () {
    #[inline(always)]
    fn from_value(input: Value) -> Result<Self, Error>
    where
        Self: Sized,
    {
        if let Value::Void = input {
            Ok(())
        } else {
            Err(Error::TypeMismatch {
                expected: Kind::Void,
                actual: input.kind(),
            })
        }
    }
}
