#![warn(
    clippy::all,
    clippy::pedantic,
    rust_2018_idioms,
    missing_docs,
    clippy::missing_docs_in_private_items
)]
#![allow(clippy::must_use_candidate, clippy::module_name_repetitions)]
//! Common object model

/// Error type
pub mod error;
/// Types and traits for methods
pub mod method;
/// General types
pub mod types;
