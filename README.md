# Ringlorn

> _adj._ the wish that the modern world felt as epic as the one depicted in old stories and
> folktales—a place of tragedy and transcendence, of oaths and omens and fates, where everyday life
> felt like a quest for glory, a mythic bond with an ancient past, or a battle for survival against
> a clear enemy, rather than an open-ended parlor game where all the rules are made up and the
> points don’t matter.

-- _The Dictionary of Obscure Sorrows_

A very early work in progress implementation of a dwarf fortress alike in Rust.
